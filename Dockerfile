FROM alpine
COPY . .
RUN apk update --quiet && apk upgrade --quiet
RUN apk --upgrade add $(cat requirements.txt)
VOLUME /tmp
RUN mvn clean
RUN mvn install
RUN cp target/spring.boot.docker.jar spring.boot.docker.jar
EXPOSE 8086

ENTRYPOINT [ "/bin/bash" ]
CMD [ "start_up.sh" ]

# ENTRYPOINT ["java","-jar","spring.boot.docker.jar"]