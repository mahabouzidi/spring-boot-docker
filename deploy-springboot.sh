#!/usr/bin/env bash

IMAGETAG="randolphcarter/springbootapp:latest"
BUILD_PUSH_IMAGE=0
DEPLOY_STACK=0
REMOVE_STACK=0
ALL_OPTS=0


build_springboot () {
	docker build -t $IMAGETAG ./spring-boot-app	
}

push_springboot () {
	docker push $IMAGETAG 
}
clean_up () {
	rm_stack
	docker volume prune -f
	docker system prune -af
}

rm_stack () {
	docker stack rm springboot-demo
	sleep 6
	docker network prune -f
}

deploy_springboot_stack () {
	sleep 3
	docker network prune -f
	docker stack deploy -c springboot-demo.yml springboot-demo
}

build_main () {
	docker login
	build_springboot
	push_springboot
}

einstampfen_und_neumachen () {
	rm_stack
	sleep 5
	clean_up
	sleep 3
	rm_stack
	build_main
	deploy_springboot_stack
	exit 0
}

mainf () {
	[ "$ALL_OPTS" = '1' ] && einstampfen_und_neumachen
	[ "$REMOVE_STACK" = '1' ] && rm_stack
	[ "$CLEAN_CACHE" -eq '1' ] && clean_up
	[ "$BUILD_PUSH_IMAGE" = '1' ] && build_main
	[ "$DEPLOY_STACK" -eq '1' ] && deploy_springboot_stack
}


[ -z $1 ] && deploy_springboot_stack && exit 0

while getopts "bcdra" opt ; do
	case "$opt" in
	b ) BUILD_PUSH_IMAGE=1  ;;
	c ) CLEAN_CACHE=1  ;;
	d ) DEPLOY_STACK=1  ;;
	r ) REMOVE_STACK=1  ;;
	a ) ALL_OPTS=1  ;;
	esac
done

mainf

exit 0
